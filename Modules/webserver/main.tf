resource "aws_default_security_group" "default-sg"{
    vpc_id = var.vpc_id

    ingress{
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }
     ingress{
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [var.route_cidr_block]
    }
    egress{
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [var.route_cidr_block]
        prefix_list_ids = []
    }
    tags = {
        Name: "${var.env_prefix}default-sg"
    }
}


resource "aws_key_pair" "ssh-key"{
    key_name = "server-key"
    public_key = file(var.my_public_key)
}

resource "aws_instance" "myapp-server"{
    ami = "ami-019f9b3318b7155c5"
    instance_type = var.instance_type

    subnet_id = var.subnet_id
    vpc_security_group_ids = [aws_default_security_group.default-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    user_data = file("entry-script.sh")


    tags = {
       Name: "${var.env_prefix}-server" 
    }

}